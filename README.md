```mermaid
graph TB

header-->main_menu_button(main menu)
header-->logo_img(logo: /)

main_menu_button--click-->main_menu_case{main_menu_case}
main_menu_case{user role}--guest-->guest_main_menu
main_menu_case--user-->user_main_menu
main_menu_case--admin-->admin_main_menu

guest_main_menu-->login_button(/login)
guest_main_menu-->register_button(/register)
guest_main_menu-->about_button(/about)


user_main_menu-->dashboard_button(/dashboard)
user_main_menu-->settings_button(/settings)
user_main_menu-->logout_button(/logout)

admin_main_menu-->dashboard_button
admin_main_menu-->settings_button
admin_main_menu-->logout_button

admin_main_menu-->users_button(/users)


footer-->footer.user_role_case{user role}
footer.user_role_case--guest-->footer.account_list((list: account))

footer.account_list-->login_button
footer.account_list-->register_button
footer.account_list-->account_help(/account-help)

footer-->about_list((list: about))
about_list-->terms_btn(/terms-and-conditions)
about_list-->privacy_btn(/privacy)
about_list-->about_us_btn(/about-us)
about_list-->our_mission_btn(/our-mission)



```
